# CMake generated Testfile for 
# Source directory: /home/antony/popf-tif/popf-tif/planner/src/popf/testcases
# Build directory: /home/antony/popf-tif/popf-tif/planner/release/popf/testcases
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("LPBFTests")
subdirs("TILTests")
subdirs("pfile1tests")
subdirs("validationtests")
subdirs("posthoctests")
subdirs("ctseffects")
subdirs("coordination")
subdirs("lpsameasstp")
subdirs("nonumbers")
subdirs("AshutoshVerma")
