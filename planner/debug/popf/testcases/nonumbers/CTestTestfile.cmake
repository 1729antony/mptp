# CMake generated Testfile for 
# Source directory: /home/antony/popf-tif/popf-tif/planner/src/popf/testcases/nonumbers
# Build directory: /home/antony/popf-tif/popf-tif/planner/debug/popf/testcases/nonumbers
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(no-num-satellite-cts-plan-reachable "/home/antony/popf-tif/popf-tif/planner/debug/popf/popf2" "-H" "-r" "-s" "-I" "-D" "-c" "-gnonumbers" "-v1" "/home/antony/popf-tif/popf-tif/planner/src/popf/testcases/nonumbers/satellite-cts-domain.pddl" "/home/antony/popf-tif/popf-tif/planner/src/popf/testcases/nonumbers/satellite-cts-problem-01.pddl" "/home/antony/popf-tif/popf-tif/planner/src/popf/testcases/nonumbers/satellite-cts-plan-01")
add_test(no-num-satellite-cts-can-be-solved "/home/antony/popf-tif/popf-tif/planner/debug/popf/popf2" "-I" "-D" "-c" "-gnonumbers" "-v1" "/home/antony/popf-tif/popf-tif/planner/src/popf/testcases/nonumbers/satellite-cts-domain.pddl" "/home/antony/popf-tif/popf-tif/planner/src/popf/testcases/nonumbers/satellite-cts-problem-01.pddl")
