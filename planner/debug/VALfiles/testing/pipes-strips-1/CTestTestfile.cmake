# CMake generated Testfile for 
# Source directory: /home/antony/popf-tif/popf-tif/planner/src/VALfiles/testing/pipes-strips-1
# Build directory: /home/antony/popf-tif/popf-tif/planner/debug/VALfiles/testing/pipes-strips-1
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(pipes-notankage-pfile1-plan-steps-should-exist "/home/antony/popf-tif/popf-tif/planner/src/VALfiles/testing/checkplanstepsexist" "/home/antony/popf-tif/popf-tif/planner/debug/VALfiles/testing/insttest" "/home/antony/popf-tif/popf-tif/planner/src/VALfiles/testing/pipes-strips-1/pipesworld-notankage_nontemporal-strips-domain.pddl" "/home/antony/popf-tif/popf-tif/planner/src/VALfiles/testing/pipes-strips-1/pipesworld-notankage_nontemporal-strips-problem.pddl" "/home/antony/popf-tif/popf-tif/planner/src/VALfiles/testing/pipes-strips-1/pipesworld-notankage_nontemporal-strips-plan")
add_test(pipes-tankage-pfile1-plan-steps-should-exist "/home/antony/popf-tif/popf-tif/planner/src/VALfiles/testing/checkplanstepsexist" "/home/antony/popf-tif/popf-tif/planner/debug/VALfiles/testing/insttest" "/home/antony/popf-tif/popf-tif/planner/src/VALfiles/testing/pipes-strips-1/pipesworld-tankage_nontemporal-strips-domain.pddl" "/home/antony/popf-tif/popf-tif/planner/src/VALfiles/testing/pipes-strips-1/pipesworld-tankage_nontemporal-strips-problem.pddl" "/home/antony/popf-tif/popf-tif/planner/src/VALfiles/testing/pipes-strips-1/pipesworld-tankage_nontemporal-strips-plan")
