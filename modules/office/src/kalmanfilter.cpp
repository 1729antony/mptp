#include "kalmanfilter.h"
using namespace std;
#include <iostream>

void KalmanFilter::start(
  const int nin, const VectorXd& xin, const MatrixXd& Pin, const MatrixXd& Fin, const MatrixXd& Qin){

  this->n = nin;
  this->I = MatrixXd::Identity(this->n, this->n);
  this->x = xin;
  this->P = Pin;
  this->F = Fin;
  this->Q = Qin;

  pose_map["s1"] = {14.775,0.325,-1.57};
  pose_map["a1"] = {14.775,0.325,-1.57};
  pose_map["a2"] = {14.775,0.325,-1.57};
  pose_map["a3"] = {14.775,0.325,-1.57};
  pose_map["b1"] = {14.775,0.325,-1.57};
  pose_map["b2"] = {14.775,0.325,-1.57};
  pose_map["b3"] = {14.775,0.325,-1.57};
  pose_map["c1"] = {14.775,0.325,-1.57};
  pose_map["c2"] = {14.775,0.325,-1.57};
  pose_map["c3"] = {14.775,0.325,-1.57};
  pose_map["l1"] = {14.775,0.325,-1.57};

  cov_map["s1"] = {0.6, 0.0, 0.0,
                  0.0, 0.6, 0.0,
                  0.0, 0.0, 0.02};
  cov_map["a1"] = {0.6, 0.0, 0.0, 0.0, 0.6, 0.0, 0.0, 0.0, 0.02};
  cov_map["a2"] = {0.6, 0.0, 0.0, 0.0, 0.6, 0.0, 0.0, 0.0, 0.02};
  cov_map["a3"] = {0.6, 0.0, 0.0, 0.0, 0.6, 0.0, 0.0, 0.0, 0.02};
  cov_map["b1"] = {0.6, 0.0, 0.0, 0.0, 0.6, 0.0, 0.0, 0.0, 0.02};
  cov_map["b2"] = {0.6, 0.0, 0.0, 0.0, 0.6, 0.0, 0.0, 0.0, 0.02};
  cov_map["b3"] = {0.6, 0.0, 0.0, 0.0, 0.6, 0.0, 0.0, 0.0, 0.02};
  cov_map["c1"] = {0.6, 0.0, 0.0, 0.0, 0.6, 0.0, 0.0, 0.0, 0.02};
  cov_map["c2"] = {0.6, 0.0, 0.0, 0.0, 0.6, 0.0, 0.0, 0.0, 0.02};
  cov_map["c3"] = {0.6, 0.0, 0.0, 0.0, 0.6, 0.0, 0.0, 0.0, 0.02};
  cov_map["l1"] = {0.6, 0.0, 0.0, 0.0, 0.6, 0.0, 0.0, 0.0, 0.02};
}

void KalmanFilter::setQ(const MatrixXd& Qin){
  this->Q = Qin;
}

void KalmanFilter::updateF(double control2, double theta ){
  double newF2 = -1*control2*sin(theta);
  if (abs(newF2) == 0 )
    newF2 = abs(newF2);
  this->F(0, 2) = newF2+0;
  this->F(1, 2) = control2*cos(theta);
}

VectorXd KalmanFilter::get(string region) const{
  auto itr = pose_map.find(region);
  VectorXd x(3);
  x(0) = itr->second.at(0);
  x(1) = itr->second.at(1);
  x(2) = itr->second.at(2);
  //this->x = x;
  return x;
}

MatrixXd KalmanFilter::getP(string region) const{
  auto itr = cov_map.find(region);
  MatrixXd Pin = MatrixXd(3, 3);
  Pin(0,0) = itr->second.at(0);
  Pin(0,1) = itr->second.at(1);
  Pin(0,2) = itr->second.at(2);

  Pin(1,0) = itr->second.at(3);
  Pin(1,1) = itr->second.at(4);
  Pin(1,2) = itr->second.at(5);

  Pin(2,0) = itr->second.at(6);
  Pin(2,1) = itr->second.at(7);
  Pin(2,2) = itr->second.at(8);

  //this->P = Pin;
  return Pin;
}
MatrixXd KalmanFilter::getF() {
  return this->F;
}

void KalmanFilter::setx(VectorXd xin, string region){
  this->x = xin;
  auto itr = pose_map.find(region);
  itr->second = {xin(0), xin(1), xin(2)};
}
void KalmanFilter::predict(){
  this->P = this->F * this->P * this->F.transpose() + this->Q;
}

void KalmanFilter::setP(MatrixXd Pin, string region){
  this->P = Pin;
  auto itr = cov_map.find(region);
  itr->second = {Pin(0,0), Pin(0,1), Pin(0,2), Pin(1,0), Pin(1,1), Pin(1,2),Pin(2,0), Pin(2,1), Pin(2,2)};

}

void KalmanFilter::update(const VectorXd& z, const MatrixXd& H, const VectorXd& Hx, const MatrixXd& R){
//cout<< "update in"<<endl;
  const MatrixXd PHt = this->P * H.transpose();//internal
  //cout<< "pht"<<endl;
  const MatrixXd S = H * PHt + R;//innovation
  //cout<< "innovation"<<endl;
  const MatrixXd K = PHt * S.inverse();//gain
    //cout<< "gain"<<endl;
 

  VectorXd y = z - Hx; //new update
  //cout<< "difference"<<endl;

  //if (y.size() == 3) y(1) = atan2(sin(y(1)), cos(y(1))); //if radar measurement, normalize angle

  this->x = this->x + K * y;// mean update
  //cout<< "mean update"<<endl;
  this->P = (this->I - K * H) * this->P;// covariance update
  //cout<< "covariance update"<<endl;
}
