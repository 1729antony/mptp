#include <iostream>
#include "OfficeSolver.h"
#include <fstream>
#include <list>
#include <bits/stdc++.h> 

using namespace std;



int main(int argc, char **argv) {
    //std::cout << "Hello, world!" << std::endl;
    OfficeSolver office;
    string problem;
    
    if (argc > 2){
        problem = argv[1];
    }else{
        cout << "Usage:\n"
        << argv[0] << " problem parameters plan" << endl;
        return 1;
    }
    string parameters[] = {0};
    office.loadSolver(parameters,1);
    map<string,double> initialState;

   // Changing variables
    initialState["(act-cost)"] = 0;
    initialState["(extern)"] = 0;
    initialState["(bound)"] = 0;
    initialState["(goal-trace)"] = 0;
    // Declaring Vector of String type 
    vector <string> region; 
  
    // Initialize vector with regions using push_back  
    // command 
    region.push_back("s1"); 
    region.push_back("a1"); 
    region.push_back("a2"); 
    region.push_back("a3"); 
    region.push_back("b1"); 
    region.push_back("b2"); 
    region.push_back("b3"); 
    region.push_back("c1"); 
    region.push_back("c2"); 
    region.push_back("c3"); 
    region.push_back("l1");
  
    // store the initial state fluents
    for (int i=0; i<region.size(); i++)   { 
    for(int j=0; j<region.size(); j++){
       if (i != j){       
        initialState["(triggered " + region[i] + " " + region[j]+")"] = 0;
       }
    }
    }   
    return 0;
}
