  /*
   <one line to give the program's name and a brief idea of what it does.>
   Copyright (C) 2015  <copyright holder> <email>
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */


#include "OfficeSolver1.h"
#include "ExternalSolver.h"
#include <map>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <algorithm>
#include "armadillo"
#include <initializer_list>
#include <cstdlib> 
#include <ctime>
using namespace std;
using namespace arma;



int trace_check = 100;
 int choice = 2;            
 // 1 for trace as cost (adding), 
 // 2 for min distance cost, 
 // 3 euclidean distance between regions as heuristic, no motion planning
 // 4 trace + euclidean distance
          

  //map <string, vector<double> > region_mapping;

extern "C" ExternalSolver* create_object(){
      return new OfficeSolver();
}

extern "C" void destroy_object(ExternalSolver *externalSolver){
      delete externalSolver;
}

OfficeSolver::OfficeSolver(){
      
}

OfficeSolver::~OfficeSolver(){
      
}

void OfficeSolver::loadSolver(string *parameters, int n){
      starting_position = "wp0";
      string Paramers = parameters[0];
      
      char const *x[]={"extern","bound"};//,"dummy1"};
      char const *y[]={"act-cost","triggered","goal-trace"};
      parseParameters(Paramers);
      affected = list<string>(x,x+2);
      dependencies = list<string>(y,y+3);
    
      string waypoint_file = "/home/asus/catkin_ws_old/popf-tif/domains/office/waypoint.txt";
      parseWaypoint(waypoint_file);

      string landmark_file = "/home/asus/catkin_ws_old/popf-tif/domains/office/landmark.txt";
      parseLandmark(landmark_file);

      string edge_file = "/home/asus/catkin_ws_old/popf-tif/domains/office/edge.txt";
      parseEdge(edge_file);

      
      startEKF();
      output_file = "/home/asus/catkin_ws_old/popf-tif/domains/office/BSP/output_motion1.txt";
      ofstream out_file;
      out_file.open(output_file,ios::out|ios::trunc);
      out_file.close();

      cost_file = "/home/asus/catkin_ws_old/popf-tif/domains/office/BSP/output_cost1.txt";
      ofstream c_file;
      c_file.open(cost_file,ios::out|ios::trunc);
      c_file.close();

      //Initialise min mapping
      region_min["s1"] = "wp0";
      region_min["a1"] = "wp1";
      region_min["a2"] = "wp3";
      region_min["a3"] = "wp5";
      region_min["b1"] = "wp7";
      region_min["b2"] = "wp9";
      region_min["b3"] = "wp11";
      region_min["c1"] = "wp13";
      region_min["c2"] = "wp15";
      region_min["c3"] = "wp17";
      region_min["l1"] = "wp19";
}

map<string,double> OfficeSolver::callExternalSolver(map<string,double> initialState,bool isHeuristic){
      
      map<string, double> toReturn;
      map<string, double>::iterator iSIt = initialState.begin();
      map<string, double>::iterator isEnd = initialState.end();
      double external;
      double cov_cost;
      double cov_bound;
      double cov_trace;
      
      goto_same_region = 0;
      map<string, double> trigger;
 
      cout<<"EXTERNAL CALL "<<endl;

      for(;iSIt!=isEnd;++iSIt){
          
          string parameter = iSIt->first;
          string function = iSIt->first;
          double value = iSIt->second;
      //cout << parameter << " " << value << endl;

          function.erase(0,1);
          function.erase(function.length()-1,function.length());
          int n=function.find(" ");
          //cout << parameter << " " << function << endl;
          if(n!=-1){
              string arg=function;
              string tmp = function.substr(n+1,5);
              
              function.erase(n,function.length()-1);
              arg.erase(0,n+1);
            if(function=="triggered"){
              trigger[arg] = value>0?1:0;
                   //cout<<"VALUE "<<value<<endl;
              if (value>0){
                cout<<"params "<<tmp<<endl;
                string from = tmp.substr(0,2);   // from and to are regions, need to extract wps (poses)
                string to = tmp.substr(3,2);

                ofstream out_file;
                                                //out_file.open(output_file);
                out_file.open(output_file,ios::out|ios::app);

                out_file << " Plan from  region "<<from << " to region "<<to<< " : read right to left \n"<<
                "-----------------------------------------------"<<"\n";
                out_file.close();
                if (from == to)
                    goto_same_region = 1;    //same region triggered dont expand, check cost function
                else{
                  if (choice == 1 || choice == 2)
                     expandPRM(from, to);
                  else if (choice == 3)
                     euclideanH(from, to);
                  else if (choice == 4){
                     expandPRM(from, to);
                     euclideanH(from, to);
                   }
                     
                }
                   

                    
 
              }
            }
          }else{
              if(function=="extern"){
                  external = value;
                  //cout << parameter << " " << value << endl;
              }else if(function=="act-cost"){
                  cov_cost = value;
               }else if(function=="bound"){
                  cov_bound = value;              
                  ////cout << parameter << " " << value << endl;
               }else if(function=="goal-trace"){
                  cov_trace = value;              
                  ////cout << parameter << " " << value << endl;
               }
          }
      }

    
      double results = calculateExtern(external, cov_cost, choice);
      if (ExternalSolver::verbose){
          cout << "(extern) " << results << endl;
      }
      //cout << "total-provability " << results.first << endl;
   //toReturn["(dummy1)"] = 0;    
      toReturn["(extern)"] = results;
      if (choice == 3 || choice == 2)
           toReturn["(bound)"] = 0;
      else
           toReturn["(bound)"] = cost_best_goal_trace;  
      //cout << endl;
      return toReturn;
}

list<string> OfficeSolver::getParameters(){
      
    return affected;
}

list<string> OfficeSolver::getDependencies(){
      
    return dependencies;
}


void OfficeSolver::parseParameters(string parameters){
  
      int curr, next;
      string line;
      ifstream parametersFile(parameters.c_str());
      if (parametersFile.is_open()){
          while (getline(parametersFile,line)){
           curr=line.find(" ");
           string region_name = line.substr(0,curr).c_str();
            curr=curr+1;
            while(true ){
              next=line.find(" ",curr);
              region_mapping[region_name].push_back(line.substr(curr,next-curr).c_str());
              if (next ==-1)
               break;
             curr=next+1;
   
            }                
          }

      }
}

double OfficeSolver::calculateExtern(double external, double cov_cost, int choice){
     //float random1 = static_cast <float> (rand())/static_cast <float>(RAND_MAX);
     double cost;
     if (goto_same_region == 1)
         cost = 10000;
     else{
         if (choice == 1 || choice == 2)
                cost = cost_function;
         else if (choice == 3){
                cost = cost_function_euclidean;
                cout<<"------------   "<<cost<<endl;
              }
         else if (choice == 4)
                cost = cost_function + cost_function_euclidean;
                  
     }

     return cost;
}

void OfficeSolver::parseWaypoint(string waypoint_file){

   int curr, next;
   string line;
   double pose1, pose2, pose3;
   ifstream parametersFile(waypoint_file);
   if (parametersFile.is_open()){
          while (getline(parametersFile,line)){
               curr=line.find("[");
               string waypoint_name = line.substr(0,curr).c_str();
     
               curr=curr+1;
               next=line.find(",",curr);

               pose1 = (double)atof(line.substr(curr,next-curr).c_str());
               curr=next+1; next=line.find(",",curr);

               pose2 = (double)atof(line.substr(curr,next-curr).c_str());
               curr=next+1; next=line.find("]",curr);

               pose3 = (double)atof(line.substr(curr,next-curr).c_str());

               if (waypoint_name == "wp0"){
                srand (static_cast <unsigned> (time(0)));
                 float r3 = -0.774 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(0.6)));
                 float r4 = -0.774 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(0.6)));
               waypoint[waypoint_name]         = vector<double> {14.775 + r3, 0.325 + r4, -1.57};

               }else    
               waypoint[waypoint_name] = vector<double> {pose1, pose2, pose3};
          }
           
    }
 
}

void OfficeSolver::parseLandmark(string landmark_file){

   int curr, next;
   string line;
   double pose1, pose2, pose3;
   ifstream parametersFile(landmark_file);
   if (parametersFile.is_open()){
          while (getline(parametersFile,line)){
               curr=line.find("[");
               string landmark_name = line.substr(0,curr).c_str();
     
               curr=curr+1;
               next=line.find(",",curr);

               pose1 = (double)atof(line.substr(curr,next-curr).c_str());
               curr=next+1; next=line.find(",",curr);

               pose2 = (double)atof(line.substr(curr,next-curr).c_str());
               curr=next+1; next=line.find("]",curr);

               pose3 = (double)atof(line.substr(curr,next-curr).c_str());
    
              landmark[landmark_name] = vector<double> {pose1, pose2, pose3};
          }
    }
 
}

void OfficeSolver::parseEdge(string edge_file){

int curr, next;
   string line;
   ifstream parametersFile(edge_file);
   if (parametersFile.is_open()){
          while (getline(parametersFile,line)){
               curr=line.find("(");
               curr=curr+1;
               next=line.find(",",curr);

               string s = line.substr(curr,next-curr).c_str();
               source.push_back(s);
           
               curr = next+1;
               next=line.find(")",curr);

               string ss = line.substr(curr,next-curr).c_str();
               target.push_back(ss);
 
          }
   }
}


void OfficeSolver::euclideanH(string from, string to){

auto mit = region_min.find(from);
start1 = mit->second;

VectorXd x(3);
VectorXd y(3);
  x(0) = waypoint[start1].at(0);
  x(1) = waypoint[start1].at(1);
  

  map<string, double> euclidean_dist;
  vector<string>::iterator nit = region_mapping[to].begin();

  for (; nit != region_mapping[to].end(); nit++){
        goal1 = *nit;
        y(0) = waypoint[goal1].at(0);
        y(1) = waypoint[goal1].at(1);
        euclidean_dist[goal1] = sqrt( (x(0)-y(0))*(x(0) - y(0)) + (x(1)-y(1))*(x(1) - y(1)) );
   }

 if (choice == 3){
map<string, double>::iterator itd = euclidean_dist.begin();
  
  string min_goal = itd->first;
  double min_cost = itd->second;
  
  for(;itd!=euclidean_dist.end();itd++){
    
     if(itd->second < min_cost){
        min_goal = itd->first; 
        min_cost = itd->second;
 }
}
 auto itmm = region_min.find(to);
  itmm->second = min_goal;
 cost_function_euclidean = min_cost;
         ofstream out_file;
         out_file.open(output_file,ios::out|ios::app);
         out_file<<min_goal<<" ---- "<<cost_function_euclidean;
out_file<<"\n"<<"--------------------------------------------------------------------------------------"<<"\n";


      out_file.close();
} else if (choice == 4){

  auto itmm = region_min.find(to);
  cost_function_euclidean = euclidean_dist[itmm->second];

}
}  

void OfficeSolver::expandPRM(string from, string to){
 //string start1, goal1;
auto itm = region_min.find(from);
start1 = itm->second;

 queue <string> to_expand;
 vector<string> expanded;
 MatrixXd P = MatrixXd(3, 3);
 VectorXd x(3);
 
 P = kalman.KF_state.getP(from);               // get the starting_node P (associated with ?from region)
 x = kalman.KF_state.get(from);                // starting pose of each node (associated with ?from region)
 
 vector<double> cost_check;
 map<string, double> cov_cost;     // for storing the costs
 
 multimap<double, string> cost1;

 map<string, double> best_goal;     // to store goal wp and the corresponding cost;
 map<string, double> best_goal_trace;  //trace of goal wp
 map<string, vector<double>> for_state;    // to set final state values
 vector<string>::iterator it = region_mapping[to].begin();

 for (; it != region_mapping[to].end(); it++){

        goal1 = *it;
       
        
        estimated_cov[start1 + goal1] = {P(0,0), P(0,1), P(0,2), P(1,0), P(1,1), P(1,2),P(2,0), P(2,1), P(2,2)};
        estimated_poses[start1 + goal1] = {x(0),x(1),x(2)};
        if (choice == 1)
            cov_cost[start1 + goal1]  = P.trace(); // trace as cost
        else if (choice == 2)
            cov_cost[start1 + goal1]  = 0;         // geometric distance as cost
        to_expand.push(start1);
        //cost1.insert({cov_cost[start1 + goal1], start1}); 

         bool run = true;
    while (run){
          
         while(to_expand.size()){
            
            //auto itco = cost1.begin();
            string current_node = to_expand.front();

            //string current_node = itco->second;
                      
            vector<string> childs = getChilds(source, target, current_node);
          
            expanded.push_back(current_node);  // adding current_node to expanded 
                         
            vector<string>::iterator itc = childs.begin();
            /*cout<<"CHILDS OF "<<current_node<<endl;
            for(auto iiii = childs.begin();iiii!=childs.end();iiii++)
               cout<<"-"<<*iiii;    
             cout<<"\n ---------------------------------"<<endl; */
            int tmp = 0;
            for (auto itest = childs.begin();itest != childs.end();itest++){
               if (find(expanded.begin(), expanded.end(), *itest) != expanded.end()) 
                  continue;
                tmp++;
            }

            for (;itc != childs.end();itc++){

               if (find(expanded.begin(), expanded.end(), *itc) != expanded.end()) // no loops for now
                  continue;

               to_expand.push(*itc);
               
               string next_node = *itc;
               
               
               predictEKF(current_node, next_node);
               
               updateEKF(next_node);

               double covariance_trace = estimated_cov[next_node + goal1].at(0) +
                                   estimated_cov[next_node + goal1].at(4) + estimated_cov[next_node + goal1].at(8);
               
               double dist = sqrt( (estimated_poses[current_node + goal1].at(0) - estimated_poses[next_node + goal1].at(0))*
                                    (estimated_poses[current_node + goal1].at(0) - estimated_poses[next_node + goal1].at(0)) +
                                     (estimated_poses[current_node + goal1].at(1) - estimated_poses[next_node + goal1].at(1))*
                                      (estimated_poses[current_node + goal1].at(1) - estimated_poses[next_node + goal1].at(1)));

               if (choice == 1)
                    cov_cost[next_node + goal1] = cov_cost[current_node + goal1] +   covariance_trace;    // trace as cost
               else if (choice == 2)
                    cov_cost[next_node + goal1] = cov_cost[current_node + goal1] +   dist;         // geometric distance as cost

              // cov_cost[next_node + goal1] = cov_cost[current_node + goal1] +   covariance_trace;                                                       
               
               //cost1.insert({cov_cost[next_node + goal1], next_node}); 
               //parent[next_node] = current_node; 
               if (next_node == goal1){                             // goal reached
                        cost_check.push_back(covariance_trace);
                        parent[next_node] = current_node; 
                        cout<<"GOAL ---- "<<next_node<<endl;
                        cout<<"parent of goal "<<current_node<<endl;
                        cout<<"TRACE "<<covariance_trace;
                        best_goal[goal1] = cov_cost[goal1 + goal1];
                        best_goal_trace[goal1] = covariance_trace;
                        run = false;
                        break;
                }
              parent[next_node] = current_node; 
            }
              
             if(!run)
                break;
             // cost1.erase(itco);
             to_expand.pop();       // removing the current_node from queue
        }
        unordered_map<string, string>::iterator itp = parent.find(goal1);
                                    //unordered_map<string, string>::iterator itp = parent.begin();
        string a = itp->first;
        string b = itp->second;

         ofstream out_file;
        // TODO keep a horizon condition and also for iterator to assert non-existence of solution, Antony
         out_file.open(output_file,ios::out|ios::app);

         ofstream c_file;
         c_file.open(cost_file,ios::out|ios::app);
  
string beg1 = a;
double trace1 = estimated_cov[a + goal1].at(0) + estimated_cov[a + goal1].at(4) + estimated_cov[a + goal1].at(4);
while (true) {

     out_file<<a<<" <- ";
     c_file<<"cov_cost "<<a<<" = "<<cov_cost[a + goal1]<<"\n \n \n";
     c_file<<"estimated_cov "<<a<<" = "<<estimated_cov[a + goal1].at(0)<<", "<<estimated_cov[a + goal1].at(1)<<
     ", "<<estimated_cov[a + goal1].at(2)<<",\n                    "<<estimated_cov[a + goal1].at(3)<<", "<<
         estimated_cov[a + goal1].at(4)<<", "<<estimated_cov[a + goal1].at(5)<<",\n                    "<<
         estimated_cov[a + goal1].at(6)<<", "<<estimated_cov[a + goal1].at(7)<<", "<<
         estimated_cov[a + goal1].at(8)<<"\n \n \n";

      c_file<<"estimated pose "<<a<<" = "<<estimated_poses[a + goal1].at(0)<<", "<<estimated_poses[a + goal1].at(1)<<
     ", "<<estimated_poses[a + goal1].at(2)<<"\n";
      c_file<<"     real pose "<<a<<" = "<<waypoint[a].at(0)<<", "<<waypoint[a].at(1)<<", "<<waypoint[a].at(2)<<
       "\n \n \n";
    if (b == start1){
        out_file<<b;
        c_file << "cov_cost " << b << " = " << cov_cost[b + goal1]<<"\n \n \n";
        c_file << "estimated_cov " << b <<" = "<<estimated_cov[b + goal1].at(0)<<", "<<estimated_cov[b + goal1].at(1)<<
        ", "<<estimated_cov[b + goal1].at(2)<<",\n                     "<<estimated_cov[b + goal1].at(3)<<", "<<
             estimated_cov[b + goal1].at(4)<<", "<<estimated_cov[b + goal1].at(5)<<",\n                    "<<
             estimated_cov[b + goal1].at(6)<<", "<<estimated_cov[b + goal1].at(7)<<", "<<
             estimated_cov[b + goal1].at(8)<<"\n \n \n";

              c_file<<"estimated pose "<<b<<" = "<<estimated_poses[b + goal1].at(0)<<", "<<estimated_poses[b + goal1].at(1)<<
     ", "<<estimated_poses[b + goal1].at(2)<<"\n";
      c_file<<"     real pose "<<b<<" = "<<waypoint[b].at(0)<<", "<<waypoint[b].at(1)<<", "<<waypoint[b].at(2)<<
       "\n \n \n";
       string beg2 = b;
double trace2 = estimated_cov[b + goal1].at(0) + estimated_cov[b + goal1].at(4) + estimated_cov[b + goal1].at(4);
         c_file<<"trace :" <<beg2 <<" -> "<< beg1<<" "<<trace1<<"\n";
        break;
    }
     a = b;
     b = parent[b];
     
    
}

  c_file<<"\n"<<"--------------------------------------------------------------------------------------"<<"\n";  
  out_file<<"\n"<<"--------------------------------------------------------------------------------------"<<"\n";


      out_file.close();
     c_file.close();
}

//Clear the arrays for storing next goal values
while (!to_expand.empty())
  {
      to_expand.pop();
  }

  expanded.clear();
  parent.clear();

}
  map<string, double>::iterator itg = best_goal.begin();
  
  string min_goal = itg->first;
  double min_cost = itg->second;
  
  for(;itg!=best_goal.end();itg++){
    
     if(itg->second < min_cost){
        min_goal = itg->first; 
        min_cost = itg->second;
 }
 }

int counter = 0;
for (double vect : cost_check){
    if (vect < trace_check)
        counter++;
}

if(counter == 0 && choice == 1 && min_goal != "wp0"){
      cout<<"NO FEASIBLE MOTION PLAN. \n"<<min_goal;
      exit(0);
}

cost_check.clear();
  cost_function = min_cost;
  //starting_position = min_goal;       // this is the new wp for starting
  auto itmm = region_min.find(to);
  itmm->second = min_goal;
  cost_best_goal_trace = best_goal_trace[min_goal];

   MatrixXd P_new = MatrixXd(3, 3);   // to set the new covariance for next call
   VectorXd x_new(3);

  P_new << estimated_cov[min_goal + min_goal].at(0), estimated_cov[min_goal + min_goal].at(1), estimated_cov[min_goal + min_goal].at(2),
        estimated_cov[min_goal + min_goal].at(3), estimated_cov[min_goal + min_goal].at(4), estimated_cov[min_goal + min_goal].at(5),
        estimated_cov[min_goal + min_goal].at(6), estimated_cov[min_goal + min_goal].at(7), estimated_cov[min_goal + min_goal].at(8);
 
  x_new(0) = estimated_poses[min_goal + min_goal].at(0);
  x_new(1) = estimated_poses[min_goal + min_goal].at(1);
  x_new(2) = estimated_poses[min_goal + min_goal].at(2);

  kalman.KF_state.setP(P_new, to);
  kalman.KF_state.setx(x_new, to);  

}

//end of expandPRM
vector<string> OfficeSolver::getChilds(vector<string> source, vector<string> target, string start_node){

vector<string> childs;
vector<string>::const_iterator its = source.begin();
vector<string>::const_iterator itt = target.begin();
//cout<<"childs "<<start_node<<"\n";
for (;its != source.end();its++)
{
  if(*its == start_node)
   childs.push_back(target.at(its-source.begin()));
}

for (;itt != target.end();itt++)
{
  if(*itt == start_node)
   childs.push_back(source.at(itt-target.begin()));
}

return childs;
}

      


void OfficeSolver::startEKF(){

  //this->timestamp = data.get_timestamp();
  VectorXd x(3);
  VectorXd alpha1(4);
  alpha1(0) = 0.05*0.05;
  alpha1(1) = 0.005*0.005;
  alpha1(2) = 0.1*0.1;
  alpha1(3) = 0.01*0.01;
  alpha = alpha1;
  kalman.dFactor = 1;
  MatrixXd P = MatrixXd(3, 3);
  MatrixXd F = MatrixXd(3, 3);
  MatrixXd Q = MatrixXd(3, 3);

  //temp stuff
  MatrixXd V = MatrixXd(3, 3);
  MatrixXd M = MatrixXd(3, 3);
 /* MatrixXd T = MartixXd(3, 3);
  MatrixXd M = MatrixXd(3, 3);*/

  //x(0)= 0; x(1)= 0; x(2)=0;
  x(0) = waypoint[starting_position].at(0);
  x(1) = waypoint[starting_position].at(1);
  x(2) = waypoint[starting_position].at(2);
  cout<<x(0)<<"---------";

  P << 0.6, 0.0, 0.0,
               0.0, 0.6, 0.0,
             0.0, 0.0, 0.02;

  cost_function = P(0,0) + P(1,1) + P(2,2);
  V<< (-1*kalman.dFactor* sin(x(2))), cos(x(2)), 0,
       kalman.dFactor* cos(x(2)), sin(x(2)), 0,
       1,0,1;

  M<< alpha(1)* kalman.dFactor* kalman.dFactor, 0, 0,
      0, alpha(2)* kalman.dFactor* kalman.dFactor, 0,
      0, 0, alpha(1)*kalman.dFactor* kalman.dFactor;



        F << 1.0, 0, (-1*kalman.dFactor*sin(x(2))),
        0,1.0,kalman.dFactor*cos(x(2)),
        0, 0, 1.0;


  Q << V*M*V.transpose();
   
   kalman.KF_state.start(3,x,P,F,Q);
   kalman.state= 0;
   ekf_state[0] = kalman;
}

void OfficeSolver::predictEKF(string current_node, string next_node)
{
      /**************************************************************************
   * PREDICTION STEP
   **************************************************************************/
  //KF.updateF(kalman.dFactor);

  MatrixXd V = MatrixXd(3, 3);
  MatrixXd M = MatrixXd(3, 3);
  MatrixXd Q = MatrixXd(3, 3);
  MatrixXd P = MatrixXd(3, 3);
  VectorXd x(3);
  VectorXd alpha1(4);
  alpha1(0) = 0.05*0.05;
  alpha1(1) = 0.005*0.005;
  alpha1(2) = 0.01*0.01;  //0.1*0.1
  alpha1(3) = 0.1*0.01;
  alpha = alpha1;

  //estimated_poses[current_node + goal1] = {estimated_poses[current_node].at(0), estimated_poses[current_node].at(1),
                                    //estimated_poses[current_node].at(2)};


  
  x(0) = estimated_poses[current_node + goal1].at(0);
  x(1) = estimated_poses[current_node + goal1].at(1);
  x(2) = estimated_poses[current_node + goal1].at(2);

   P << estimated_cov[current_node + goal1].at(0), estimated_cov[current_node + goal1].at(1), estimated_cov[current_node + goal1].at(2),
        estimated_cov[current_node + goal1].at(3), estimated_cov[current_node + goal1].at(4), estimated_cov[current_node + goal1].at(5),
        estimated_cov[current_node + goal1].at(6), estimated_cov[current_node + goal1].at(7), estimated_cov[current_node + goal1].at(8);
  
  vector<double> state = waypoint[next_node];


  double control1 = 0;
  double control2 = sqrt((state.at(0)-x(0)) *(state.at(0)-x(0)) + (state.at(1)-x(1))*(state.at(1)-x(1)));
  double control3 = 0;

    
  if( (x(0) - state.at(0)) == 0 && (x(1) - state.at(1)) == 0)
        control1 = atan2(state.at(1),state.at(0)) - x(2);
  else
      control1 = atan2(state.at(1) - x(1), state.at(0) - x(0)) - x(2);

  control3 = state.at(2) - x(2) - control1 ;
   //control3 = check_angle(control3);
  x(2) = x(2) + control1;

  kalman.KF_state.updateF(control2, x(2));

  //cout<<"new F"<<kalman.KF_state.getF()<<endl;

  V<< -1*control2* sin(x(2)), cos(x(2)), 0,
       control2* cos(x(2)), sin(x(2)), 0,
       1,0,1;

 /* M<< alpha(0)* control1* control1+ alpha(1)* control2* control2, 0, 0,
      0, alpha(2)* control2* control2+ alpha(3)*((control1*control1)+(control3*control3)), 0,
      0, 0, alpha(1)*control2* control2+ alpha(0)*control3* control3; */

  M<< alpha(0)* abs(control1) + alpha(1)* control2, 0, 0,
      0, alpha(2)* control2+ alpha(3)*(abs(control1)+abs(control3)), 0,
      0, 0, alpha(1)*control2+ alpha(0)*abs(control3);

  Q = V*M*V.transpose();
   
   
  x(0) = x(0) + control2*cos(x(2));

  x(1) = x(1)+ control2*sin(x(2));
  x(2) = x(2)+ control3;
 
  x(2) = check_angle(x(2));
  //cout<<"x-new "<<x<<endl;
  //cout<<" next node estimate "<<x(0)<<" "<<x(1)<<endl;
  //cout<<" ---------------------------- \n";
  
  MatrixXd F =  kalman.KF_state.getF() ;

  P = F * P * F.transpose() + Q;
 
 estimated_poses[next_node + goal1]  = {x(0), x(1), x(2)};
 estimated_cov[next_node + goal1] = {P(0,0), P(0,1), P(0,2), P(1,0), P(1,1), P(1,2),P(2,0), P(2,1), P(2,2)};
 //cout<<" estimated pose  -- "<< next_node <<"  "<< x(0)<<" "<<x(1)<<endl;
}

double OfficeSolver::check_angle(double angle)
{
  while( angle< - M_PI)
  {  angle = angle+2*M_PI;
  }
  while( angle>  M_PI)
  {  angle = angle-2*M_PI;
  }
  return angle;

}

void OfficeSolver::updateEKF(string next_node)
{
    /**************************************************************************
   * UPDATE STEP
   **************************************************************************/
 
  VectorXd Hx(2);
  MatrixXd R = MatrixXd(2, 2);
  MatrixXd H = MatrixXd(2, 3);
  VectorXd z(2);
  MatrixXd P = MatrixXd(3, 3);
  VectorXd x(3);
  MatrixXd I = MatrixXd::Identity(3, 3);
  
 
  R<< 0.025, 0,
      0, 0.001;

srand (static_cast <unsigned> (time(0)));
  
  x(0) = estimated_poses[next_node + goal1].at(0);
  x(1) = estimated_poses[next_node + goal1].at(1);
  x(2) = estimated_poses[next_node + goal1].at(2);
  //string destination = kalman.to;
  //vector<double> landmark = landmarks[destination];

  for(auto itl = landmark.begin(); itl!= landmark.end();itl ++){
   vector<double> lm = itl->second;
   double range = sqrt((lm.at(0)-x(0))*(lm.at(0)-x(0))+ ((lm.at(1)-x(1))* (lm.at(1)-x(1))));
   double bearing = atan2((lm.at(1)-x(1)),(lm.at(0)-x(0)))- x(2); 
   
   if (range < 7 && abs(bearing) < 3*M_PI/4){
    H<< -(lm.at(0)-x(0))/range, -(lm.at(1)-x(1))/range, 0,
      (x(1)-lm.at(1))/(range*range), -(x(0)-lm.at(0))/(range *range), -1;

    float random1 = static_cast <float> (rand())/static_cast <float>(RAND_MAX);
    float random2 = static_cast <float> (rand())/static_cast <float>(RAND_MAX);

    z(0) = range+ random1/2;   //12
    z(1) = bearing+ random2* M_PI/12;


    Hx(0) = range;
    Hx(1) = bearing;


  P << estimated_cov[next_node + goal1].at(0), estimated_cov[next_node + goal1].at(1), estimated_cov[next_node + goal1].at(2),
        estimated_cov[next_node + goal1].at(3), estimated_cov[next_node + goal1].at(4), estimated_cov[next_node + goal1].at(5),
        estimated_cov[next_node + goal1].at(6), estimated_cov[next_node + goal1].at(7), estimated_cov[next_node + goal1].at(8);

  MatrixXd PHt = P * H.transpose();
  MatrixXd S = H * PHt + R;             //innovation
  //cout<< "innovation"<<endl;
  MatrixXd K = PHt * S.inverse();      //gain
    //cout<< "gain"<<endl;
 

  VectorXd y = z - Hx;                 //new update
  //cout<< "difference"<<endl;

  //if (y.size() == 3) y(1) = atan2(sin(y(1)), cos(y(1))); //if radar measurement, normalize angle

  x = x + K * y;                       // mean update
  //cout<< "mean update"<<endl;
  P = (I - K * H) * P;                 // covariance update

  estimated_poses[next_node + goal1]  = {x(0), x(1), x(2)};
  estimated_cov[next_node + goal1] = {P(0,0), P(0,1), P(0,2), P(1,0), P(1,1), P(1,2),P(2,0), P(2,1), P(2,2)};
   }

  }

  //cout<< "covariance update"<<endl;
 
  

  //kalman.KF_state.update(z, H, Hx, R);

}

