/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2015  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef OFFICESOLVER_H
#define OFFICESOLVER_H

#include "ExternalSolver.h"
#include "kalmanfilter.h"
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <queue>
#include <unordered_map>

using namespace std;

struct EKF{
    double relativedistance;
    double finalTrace;
    double dFactor;
    double distance;
    KalmanFilter KF_state;
    string from;
    string to;
    int times_predicted;
    int no_of_predictions;
    bool extra;
    int state; //current
    bool moving;


};

class OfficeSolver : public ExternalSolver
{
public:
    OfficeSolver();
    ~OfficeSolver();
    virtual void loadSolver(string* parameters, int n);
    virtual map<string,double> callExternalSolver(map<string,double> initialState, bool isHeuristic);
    virtual  list<string> getParameters();
    virtual  list<string> getDependencies();
    map<string, vector<double>> waypoint;
    map<string, vector<double>> landmark;
    map<string, vector<double>> estimated_poses; //for estimated poses
    map<string, vector<double>> estimated_cov;
    unordered_map<string, string> parent;        // to store the parent node, so as to recover the path
    string start1, goal1;
   

    map<string, string> region_min;              //to store min wp for each region

    double cost_function, cost_best_goal_trace, cost_function_euclidean;
    int goto_same_region;
   
    void parseWaypoint(string waypoint_file);
    void parseLandmark(string landmark_file);
    void parseEdge(string edge_file);
    map<string, vector<string>> region_mapping;
    vector <string> source, target; 
    string starting_position;
    EKF kalman;
   
    map<unsigned long,EKF> ekf_state; //store EKF for each state


    void euclideanH(string from, string to);
    void parseParameters(string parameters);
    void predictEKF(string current_node, string next_node);
    void updateEKF(string next_node);
    void updateQ();
    void startEKF();
    
    double check_angle(double angle);
    double discretization2;
    string output_file;
    string cost_file;
    VectorXd alpha;   

    vector<string> getChilds(vector<string> source, vector<string> target, string start_node);



private:
    list<string> affected;
    list<string> dependencies;
    //void parseParameters(string parameters);
    //map<string,list<string> > destinations;
    //map<string,int> cities;
    //map<string, double> gamma;
    double calculateExtern(double external, double total_cost, int choice);
    void expandPRM(string from, string to);
    vector<string> findParameters(string line, int&n);

};

#endif // TESTSOLVER_H
