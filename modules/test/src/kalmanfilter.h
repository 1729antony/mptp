#ifndef KALMANFILTER_H_
#define KALMANFILTER_H_

#include </home/asus/catkin_ws/eigen/Eigen/Dense>

using Eigen::MatrixXd;
using Eigen::VectorXd;

class KalmanFilter{

  private:
    int n;
    VectorXd x;
    MatrixXd P;
    MatrixXd F;
    MatrixXd Q;
    MatrixXd I;



  public:
    KalmanFilter(){};
    void start(const int nin, const VectorXd& xin, const MatrixXd& Pin, const MatrixXd& Fin, const MatrixXd& Qin);
    void setQ(const MatrixXd& Qin);
    void setx(VectorXd xin);
    void setP(MatrixXd Pin);
    void updateF(double control2, double theta);
    VectorXd get() const;
    MatrixXd getP() const;
    MatrixXd getF(); 
    void predict();
    void update(const VectorXd& z, const MatrixXd& H, const VectorXd& Hx, const MatrixXd& R);
    MatrixXd T;
    MatrixXd M;
};


#endif /* KALMANFILTER_H_ */
