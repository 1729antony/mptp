#include "kalmanfilter.h"
using namespace std;
#include <iostream>

void KalmanFilter::start(
  const int nin, const VectorXd& xin, const MatrixXd& Pin, const MatrixXd& Fin, const MatrixXd& Qin){

  this->n = nin;
  this->I = MatrixXd::Identity(this->n, this->n);
  this->x = xin;
  this->P = Pin;
  this->F = Fin;
  this->Q = Qin;
}

void KalmanFilter::setQ(const MatrixXd& Qin){
  this->Q = Qin;
}

void KalmanFilter::updateF(double control2, double theta ){
  double newF2 = -1*control2*sin(theta);
  if (abs(newF2) == 0 )
    newF2 = abs(newF2);
  this->F(0, 2) = newF2+0;
  this->F(1, 2) = control2*cos(theta);
}

VectorXd KalmanFilter::get() const{
  return this->x;
}

MatrixXd KalmanFilter::getP() const{
  return this->P;
}
MatrixXd KalmanFilter::getF() {
  return this->F;
}

void KalmanFilter::setx(VectorXd xin){
  this->x = xin;
}
void KalmanFilter::predict(){
  //this->x = this->F * this->x;
  //this->x = this->x 
  this->P = this->F * this->P * this->F.transpose() + this->Q;
  //this->P = this->F * this->P * this->F.transpose() ;
  /*cout<< P(0,0)<<endl;
  cout <<P(1,1)<<endl;
  cout <<P(2,2)<< endl;
  cout <<"next trace of predict"<<endl;*/
}

void KalmanFilter::setP(MatrixXd Pin){
  this->P = Pin;
}

void KalmanFilter::update(const VectorXd& z, const MatrixXd& H, const VectorXd& Hx, const MatrixXd& R){
//cout<< "update in"<<endl;
  const MatrixXd PHt = this->P * H.transpose();//internal
  //cout<< "pht"<<endl;
  const MatrixXd S = H * PHt + R;//innovation
  //cout<< "innovation"<<endl;
  const MatrixXd K = PHt * S.inverse();//gain
    //cout<< "gain"<<endl;
 

  VectorXd y = z - Hx; //new update
  //cout<< "difference"<<endl;

  //if (y.size() == 3) y(1) = atan2(sin(y(1)), cos(y(1))); //if radar measurement, normalize angle

  this->x = this->x + K * y;// mean update
  //cout<< "mean update"<<endl;
  this->P = (this->I - K * H) * this->P;// covariance update
  //cout<< "covariance update"<<endl;
}
