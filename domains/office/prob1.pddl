(define (problem prob01)
(:domain office)
(:objects
     s1 a1 a2 a3 b1 b2 b3 c1 c2 c3 l1 - region
     kenny - robot
)
(:init
    (robot_in kenny s1)

    (= (act-cost) 0)
    (= (execution-time) 0)
    (= (extern) 0)
    (= (cov-cost) 0)
    (= (prepared) 0)
    (= (bound) 0)
    (= (goal-trace) 0)

    (= (get s1) 0)
    (= (get a1) 1)
    (= (get a2) 1)
    (= (get a3) 0)
    (= (get b1) 0)
    (= (get b2) 0)
    (= (get b3) 0)
    (= (get c1) 0)
    (= (get c2) 0)
    (= (get c3) 0)

 
)
(:goal 
     (and (reached l1)  (collected a2) (collected a1) 
          (>= (act-cost) 0) (< (goal-trace) 3) )
)
(:metric minimize (+ (act-cost) (goal-trace)))
)

;;(reviewed a3) (reviewed b1)  (< (goal-trace) 2)
;;  (reviewed a3) (reviewed b1) (reviewed a1) (reviewed b3)
        ;;  (reviewed c1) (reviewed c2) (reviewed c3) 

;; (collected c1)(collected b1)(collected a1)
       ;;  (collected c3)(collected b3)(collected c2)


         ;; (collected c1)(collected b1)(collected a1)
         ;;(collected c3)

         ;;(collected c1)(collected a1)
