(define (domain office)

(:requirements :typing :durative-actions :numeric-fluents :negative-preconditions :action-costs :conditional-effects :equality :fluents )


(:types 	robot region 
)

(:predicates
		(robot_in ?v - robot ?r - region) (reached ?r - region)
	     (reviewed ?r - region) (collected ?r - region)

)

(:functions 
		(act-cost) (execution-time) (prepared) (get ?r - region) (goal-trace)
          (extern) (cov-cost) (triggered ?from ?to - region) (bound)
)

(:durative-action goto_region
		:parameters (?v - robot ?from ?to - region)
		:duration (= ?duration 100)
		:condition (and (at start (robot_in ?v ?from)))
	     :effect (and (at start (not (robot_in ?v ?from))) (at start (increase (triggered ?from ?to) 1))
		(at end (robot_in ?v ?to)) (at end (assign (triggered ?from ?to) 0))	
           (at end (increase (act-cost) (extern))) (at end (increase (goal-trace) (bound))))
)


(:durative-action collect_document
		:parameters (?v - robot ?r - region)
		:duration (= ?duration 20)
		:condition (and (at start (robot_in ?v ?r)) (at start (> (get ?r) 0))
          (over all (robot_in ?v ?r)))
	     :effect (and (at end (collected ?r))
          (at end (increase (act-cost) 4)))
)


(:durative-action goto_lift
		:parameters (?v - robot ?from ?to - region)
		:duration (= ?duration 100)
		:condition (and (at start (robot_in ?v ?from))  )
	     :effect (and (at start (not (robot_in ?v ?from))) (at start (increase (triggered ?from ?to) 1))
		(at end (reached ?to)) (at end (assign (triggered ?from ?to) 0))	
           (at end (increase (act-cost) (extern))))
)

)

