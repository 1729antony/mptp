(define (domain office)

(:requirements :typing :durative-actions :numeric-fluents :negative-preconditions :action-costs :conditional-effects :equality :fluents )


(:types 	robot region lift
)

(:predicates
	;;	(robot_at ?v - robot ?wp - waypoint) (visited ?wp - waypoint) (visited ?r - region)
		(robot_in ?v - robot ?r - region) (reached ?l - lift)
	      (reviewed ?at ?from - region) 
          (todeliver ?at ?from1 ?from2 ?from3 - region)
)

(:functions 
		(total-cost)  (collected ?r - region) (reviewer ?r - region) (execution-time)
          (extern) (triggered ?from ?to - region) 
)

(:durative-action goto_region
		:parameters (?v - robot ?from ?to - region)
		:duration (= ?duration 5)
		:condition (and (at start (robot_in ?v ?from)))
	     :effect (and (at start (not (robot_in ?v ?from))) (at start (increase (triggered ?from ?to) 1))
		(at end (robot_in ?v ?to)) (at end (assign (triggered ?from ?to) 0))	
          (at end (increase (total-cost) (extern)))
		(at end (increase (execution-time) 5))) 
)


(:durative-action collect_document
		:parameters (?v - robot ?r - region)
		:duration (= ?duration 1)
		:condition (and (at start (robot_in ?v ?r)) (at start (>= (collected ?r) 1))
           (over all (robot_in ?v ?r)))
	     :effect (and (at end (increase (collected ?r) 1))
          (at end (increase (total-cost) 6))
		(at end (increase (execution-time) 1)))
)

(:durative-action prepare_document
		:parameters (?v - robot ?at ?from - region)
		:duration (= ?duration 4)
		:condition (and (at start (robot_in ?v ?at)) (at start (= (reviewer ?at) 1)) (at start (> (collected ?from) 1)))
	     :effect (and (at end (reviewed ?at ?from))
          (at end (increase (total-cost) 10))
		(at end (increase (execution-time) 4)))
)

(:durative-action submit_document
		:parameters (?v - robot ?at ?from1 ?from2 ?from3 - region)
		:duration (= ?duration 1)
		:condition (and (at start (robot_in ?v ?at)) (at start (not (= ?from1 ?from2))) (at start (not (= ?from1 ?from3))) 
		(at start (not (= ?from2 ?from3))) 
          (at start (reviewed ?at ?from1)) (at start (reviewed ?at ?from2)) (at start (reviewed ?at ?from3))
    		(over all (robot_in ?v ?at)))
	     :effect (and (at end (todeliver ?at ?from1 ?from2 ?from3))
          (at end (increase (total-cost) 7))
		(at end (increase (execution-time) 1)))
)


(:durative-action goto_lift
		:parameters (?v - robot ?at ?from1 ?from2 ?from3 - region ?l - lift)
		:duration (= ?duration 3)
		:condition (and (at start (robot_in ?v ?at)) (at start (todeliver ?at ?from1 ?from2 ?from3)))
	     :effect (and (at end (reached ?l))
          (at start (not (robot_in ?v ?at)))
          (at end (increase (total-cost) 5))
		(at end (increase (execution-time) 3)))
)
)

