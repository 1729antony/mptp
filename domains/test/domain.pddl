(define (domain office)

(:requirements :typing :durative-actions :numeric-fluents :negative-preconditions :action-costs :conditional-effects :equality :fluents)

(:types 	robot region
)

(:predicates
	;;	(robot_at ?v - robot ?wp - waypoint) (visited ?wp - waypoint) 
          (visited ?r - region)
		(robot_in ?v - robot ?r - region) 
)

(:functions 
		(total-cost) (extern) (triggered ?from ?to - region) 
)
(:durative-action goto_region
		:parameters (?v - robot ?from ?to - region)
		:duration (= ?duration 5)
		:condition (and (at start (robot_in ?v ?from)) )
	     :effect (and (at start (not (robot_in ?v ?from))) (at start (increase (triggered ?from ?to) 1))
		(at end (robot_in ?v ?to)) 	(at end (assign (triggered ?from ?to) 0))
          (at end (increase (total-cost) (extern)))
		(at end (visited ?to))) 
)
)


